# Modules of SAP-1

## Clock Module

This sets the input clock with various speeds/methods:
    - Adjustable speed. Adjust clock between different speeds
    - Manual mode. Progress clock one cycle at a time

clk (
    clk_in : input, 1 bit
    mode_in : input, 1bit
    next : input, 1 bit
    speed : input, 8 bit
    clk_out : output, 1 bit
)

## Registers

The CPU has three 8-Bit registers. Two general purpose registers,  A and B. And
also one instruction register, IR.

A - reads from BUS, writes to ALU
    AI - A input enable
    AO - A output enable
B - reads from BUS, writes to ALU
    BI - B input enable
    BO - B output enable
IR - reads from BUS, writes to control Logic

reg (
    clk : input
    rst : input
    en : input
    wr : input
    addr : input, 4 bit
    din : input, 8 bit
    dout : output, 8 bit
)

## Memory Address Register (MAR)

mar (
    clk : input
    rst : input
    prog : input
    bus : input, 4 bit
    addr : output, 4bit
)


## Random Access Memory (RAM)

The original breadboard version uses only 4-bit adresses which can only access
16 bytes of RAM. 
TODO: Think if this can be extended to 8 or even 16 bit.

mem (
    clk : input
    rst : input
    en : input
    wr : input
    addr : input, 4 bit
    din : input, 8 bit
    dout : output, 8 bit
)


## Program counter (PC)

pc (
    clk : input
    rst : input
    en : input
    j : input
    jaddr : input, 4 bit
    pc : output, 4 bit
)

## Output Register

Shows the data on a 7 segment display. Does not write back to the bus

outreg (
    clk : input
    rst : input
    en : input
    addr : input, 4 bit
    din : input, 8 bit
)

## Arithmetic Logic Unit (ALU)

- EO - output enable
- SU - subtract signal

alu (
    clk : input
    ans : input -- add, not subtract
    a : input, 8 bit
    b : input, 8 bit
    y : output, 8 bit
)


## Microinstruction Counter

minstCounter (
    nclk : input
    rst : input
    en : input
    mcnt : output, 3 bit
    step : output, 5 bit - one hot encoded  microsteps
)

## Instruction Decoder

cw[16 .. 0] = (HLT, MI, RI, RO, IO, II, AI, AO,  EO, SU, BI, OI, CE, CO, JP, FI)

HLT - halt counter
MI - Memory address in
RI - RAM in
RO - RAM out
IO - Instruction data out
II - Instruction register in
AI - Register A in 
AO - Register A out

EO - ALU out
SU - ALU subtract
BI - Register B in
OI - Output Latch
CE - Counter enable
CO - Counter out
JP - Jump
FI - Flag register in

idecode (
    instr : input, 4 bit
    mcnt : input 3 bit
    zf : input
    cf : input
    cw : output, 16 bit
)

## Control Logic

ctrl (
    clk : input
    rst : input
    ir : input, 4 bit
)

## Reference

See the eater page for reference: ![8-bit computer](https://eater.net/8bit)

