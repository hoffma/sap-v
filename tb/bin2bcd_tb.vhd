library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bin2bcd_tb is
end entity;

architecture Behave of bin2bcd_tb is
    signal i_clk : std_logic := '0';
    signal bin_i : std_logic_vector(7 downto 0) := (others => '0');
    signal h_o, t_o, o_o : std_logic_vector(3 downto 0);
begin

    i_clk <= not i_clk after 10 ns;

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            bin_i <= std_logic_vector(unsigned(bin_i) + 1);
        end if;
    end process;

    inst_bcd : entity work.bin2bcd
    port map (
        bin => bin_i,
        hundreds => h_o,
        tens => t_o,
        ones => o_o
    );

end Behave;
