library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cpu_tb is
end entity;


architecture Behave of cpu_tb is
    signal clk_i, s_rst : std_logic := '0';
    signal led : std_logic_vector(7 downto 0);
begin

    clk_i <= not clk_i after 10 ns;
    s_rst <= '1', '0' after 20 us;

    inst_cpu : entity work.cpu
    port map (
        clk => clk_i,
        rst => s_rst,
        led => led
    );

end Behave;
