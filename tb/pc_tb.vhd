-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;

entity pc_tb is
end entity;


architecture Behav of pc_tb is
    signal i_clk : std_logic := '0';
    signal rst, en, cs, jmp : std_logic;
    signal jaddr, pc_pc : std_logic_vector(3 downto 0);
begin

    i_clk <= not i_clk after 20 ns;
    rst <= '1', '0' after 100 ns;
    en <= '0', '1' after 50 ns, '0' after 150 ns, '1' after 200 ns;
    jmp <= '0', '1' after 600 ns, '0' after 640 ns;
    cs <= '0', '1' after 200 ns, '0' after 300 ns, '1' after 400 ns;

    jaddr <= "1000";

    inst_pc : entity work.pc port map
    (
        clk => i_clk,
        rst => rst,
        en => en,
        cs => cs,
        jmp => jmp,
        jaddr => jaddr,
        pc_out => pc_pc
    );

end Behav;
