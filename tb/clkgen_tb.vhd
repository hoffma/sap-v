library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clkgen_tb is
end entity;

architecture Behave of clkgen_tb is
    signal clk_i, en, mode, step, clk_o : std_logic := '0';
    signal div : std_logic_vector(15 downto 0);
begin

    clk_i <= not clk_i after 10 ns;
    en <= '0', '1' after 50 ns;
    mode <= '0', '1' after 500 ns;
    step <= '0', '1' after 600 ns, '0' after 650 ns, '1' after 700 ns, 
                '0' after 750 ns, '1' after 800 ns, '0' after 850 ns;
    div <= x"0003";

    inst_clk : entity work.clkgen
    port map (
        clk_in => clk_i,
        en => en,
        mode => mode,
        step => step,
        clkdv => div,
        clk_out => clk_o
    );

end Behave;
