library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity control_tb is
end entity;

architecture Behave of control_tb is
    signal i_clk : std_logic := '0';
    signal CE, rst, cf, zf : std_logic;
    signal instr : std_logic_vector(3 downto 0);
begin

    i_clk <= not i_clk after 10 ns;
    rst <= '1', '0' after 100 ns;
    cf <= '1';
    zf <= '0';

    process(i_clk)
    begin
        if rising_edge(i_clk) then
            if CE = '1' then
                instr <= std_logic_vector(unsigned(instr) + 1);
            end if;
        end if;

        if rst = '1' then
            instr <= (others => '0');
        end if;
    end process;

    inst_dut : entity work.control port map
    (
        clk => i_clk,
        rst => rst,
        instr => instr,
        cf => cf,
        zf => zf,
        CE => CE
    );

end Behave;
