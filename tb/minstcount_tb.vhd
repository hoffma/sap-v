library ieee;
use ieee.std_logic_1164.all;

entity minstcount_tb is
end entity;


architecture Behav of minstcount_tb is
    signal i_clk, s_rst, en : std_logic := '0';
begin

    i_clk <= not i_clk after 10 ns;
    s_rst <= '1', '0' after 100 ns, '1' after 250 ns, '0' after 270 ns;
    en <= '0', '1' after 200 ns, '0' after 300 ns, '1' after 350 ns;

    dut : entity work.minstcount port map
    (
        nclk => i_clk,
        rst => s_rst,
        en => en
    );

end Behav;
