library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_tb is
end entity;


architecture Behave of ram_tb is
    signal clk_i : std_logic := '0';
    signal state : std_logic_vector(1 downto 0) := "00";
    signal rst, RI, RO : std_logic := '0';
    signal addr : std_logic_vector(3 downto 0) := (others => '0');
    signal data_in, data_out, data_bus : std_logic_vector(7 downto 0) := (others => '0');
begin

    clk_i <=  not clk_i after 10 ns;
    rst <= '1', '0' after 100 ns;

    data_bus <= data_in when (RO = '0' and RI = '1') else data_out;

    process(clk_i)
    begin
        if rising_edge(clk_i) then
            state <= std_logic_vector(unsigned(state) + 1);

            case state is
                when "00" => 
                    RO <= '1';
                    RI <= '0';
                    addr <= "0001";

                when "01" => 
                    RO <= '1';
                    RI <= '0';
                    addr <= "0010";
                when "10" => 
                    RO <= '1';
                    RI <= '0';
                    addr <= "0011";
                when "11" => 
                    data_in <= std_logic_vector(unsigned(data_in) + 1);
                    RO <= '0';
                    RI <= '1';
                    addr <= "0001";
                when others =>
                    state <= "00";
            end case;

            if rst = '1' then
                RO <= '0';
                RI <= '0';
                addr <= "0000";
                state <= "00";
                data_in <= x"aa";
            end if;
        end if;
    end process;

    inst_ram : entity work.ram 
    port map (
        clk => clk_i,
        rst => rst,
        oe => RO,
        we => RI,
        addr => addr,
        din => data_in,
        dout => data_out
    );

end Behave;
