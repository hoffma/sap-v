library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debounce_tb is
end entity;

architecture Behave of debounce_tb is
    signal clk_i, btn_i, btn_o : std_logic := '0';
begin

    clk_i <= not clk_i after 5 ns;
    btn_i <= '0', '1' after 20 ns, '0' after 40 ns, '1' after 100 ns, '0' after 20 us;

    inst_debounce : entity work.debounce 
    port map (
        clk => clk_i,
        btn_in => btn_i,
        btn_out => btn_o
    );

end Behave;
