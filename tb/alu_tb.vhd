-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;

entity alu_tb is
end entity;


architecture Behav of alu_tb is
    constant DWIDTH : integer := 8;

    signal i_clk : std_logic := '0';
    signal cs, ans : std_logic;
    signal a, b, y : std_logic_vector(DWIDTH-1 downto 0);

begin
    i_clk <= not i_clk after 20 ns;

    inst_alu : entity work.alu
    generic map (
        DATA_WIDTH => DWIDTH
    )
    port map (
        clk => i_clk,
        cs => cs,
        ans => ans,
        a => a,
        b => b,
        y => y
    );

    stim: process
    begin
        a <= x"ff";
        b <= x"03";
        ans <= '1';
        cs <= '0';
        wait for 200 ns;

        cs  <= '1';
        wait for 200 ns;
        a <= x"05";
        wait for 200 ns;

        ans <= '0';
        wait for 200 ns;
        a <= x"03";
        wait for 200 ns;

        assert false report "Simulation finished." severity error;
    end process;

end Behav;
