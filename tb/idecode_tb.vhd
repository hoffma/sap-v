library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity idecode_tb is
end entity;


architecture Behav of idecode_tb is
    signal i_clk, i_nclk, rst, en : std_logic := '0';

    signal instruction_count : std_logic_vector(3 downto 0) := (others => '0');
    signal micro_count : std_logic_vector(2 downto 0) := (others => '0');
    signal count : std_logic_vector(6 downto 0) := (others => '0');
    signal zf, cf : std_logic;
    signal control_word : std_logic_vector(15 downto 0);
begin

    i_clk <= not i_clk after 10 ns;
    i_nclk <= not i_clk;
    rst <= '1', '0' after 100 ns;

    cf <= '1';
    zf <= '0';

    micro_count <= count(2 downto 0);
    instruction_count <= count(6 downto 3);


    process(i_clk)
    begin
        if rising_edge(i_clk) then
            count <= std_logic_vector(unsigned(count) + 1);

            if rst = '1' then
                -- count <= (others => '0');
                count <= "0111000";
            end if;
        end if;
    end process;

    inst_idecode : entity work.idecode port map
    (
        instr => instruction_count,
        mcnt => micro_count,
        zf => zf,
        cf => cf,
        cw => control_word
    );

end Behav;
