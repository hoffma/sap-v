-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;

entity reg_tb is
end entity;

architecture Behav of reg_tb is
    constant DWIDTH : integer := 8;

    signal i_clk : std_logic := '0';
    signal rst, cs, wr : std_logic;
    signal din, dout : std_logic_vector(DWIDTH-1 downto 0);
begin

    i_clk <= not i_clk after 20 ns;
    rst <= '1', '0' after 100 ns;

    stim: process
    begin
        din <= x"aa";
        cs  <= '0';
        wr <= '0';
        wait for 200 ns;

        cs  <= '1';
        wr <= '0';
        wait for 200 ns;

        cs  <= '0';
        wr <= '1';
        wait for 200 ns;

        cs  <= '1';
        wr <= '0';
        wait for 200 ns;

        din <= x"55";
        cs  <= '1';
        wr <= '1';
        wait for 200 ns;

        cs  <= '0';
        wr <= '0';
        wait for 200 ns;

        assert false report "Simulation finished." severity error;
    end process;

    inst_reg : entity work.reg 
    generic map(
        DATA_WIDTH => DWIDTH
    )
    port map (
        clk => i_clk,
        rst => rst,
        cs => cs,
        wr => wr,
        din => din,
        dout => dout
    );

end Behav;
