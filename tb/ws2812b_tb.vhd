library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ws2812b_tb is
end entity;

architecture Behave of ws2812b_tb is
    constant NUM_LEDS : integer := 4;
    constant NUM_LEDS_LOG2 : integer := 2;

    signal clk_i, dout : std_logic := '0';
    signal addr : std_logic_vector(7 downto 0) := (others => '0');
    signal colour : std_logic_vector(23 downto 0);
begin
    clk_i <= not clk_i after 20 ns;

    colour <= x"00aa00" when addr(1) = '0' else
              x"110011";

    inst_ws : entity work.ws2812b port map
    (
        clk25 => clk_i,
        addr => addr,
        colour => colour,
        dout => dout
    );

end Behave;
