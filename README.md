# SAP-V(HDL),  Ben Eater CPU in VHDL

Ben Eaters breadboard computer in VHDL. Not tested on hardware yet, but it counts...

![fibonacci numbers](./pics/fib.png)



Program can be changed by changed the firmware-file in ram.vhd

- Folder ```hdl/``` contains all the CPU sources
- Folder ```tb/``` contains all the testbenches
- Folder ```asmTools/``` contains the (sligthly modified) Assembler from SAP-1[2]

## Starting the simulation

- Type ```make sim```
- Run ```gtkwave cpu_sim.ghk```
- ctrl-o -> load cpu_waveform.gtkw

## Assemble program

Changed it from the original version so that it outputs binary strings which can be easily read with VHDL

```
cd asmTools
./sap-asm Count40.s > firmware.dat
```

For visualization of RAM just do

```
./sap-asm Count40.s
```




Links:

- [1] Ben Eaters Website: https://eater.net/8bit/
- [2] A simple 8-bit micro-processor using mostly discrete logic chips. https://github.com/dangrie158/SAP-1

