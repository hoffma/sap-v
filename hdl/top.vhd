library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    Port (
        clk_25mhz : in std_logic;
        btn : in std_logic_vector(3 downto 0);
        gp : out std_logic_vector(13 downto 0);
        led : out std_logic_vector(7 downto 0);

        wifi_gpio0 : out std_logic
    );
end entity;

architecture Behave of top is
    type t_REG_STATE is record
        state : std_logic;
        colour : std_logic_vector(23 downto 0);
    end record t_REG_STATE;

    type t_STATE_ARRAY is array(0 to 3) of t_REG_STATE;

    signal colour : std_logic_vector(23 downto 0);
    signal count : std_logic_vector(31 downto 0) := (others => '0');
    signal addr_int : integer;
    signal addr : std_logic_vector(7 downto 0);

    signal led_state : std_logic_vector(3 downto 0);

    signal state_data : t_STATE_ARRAY;
    signal bcd_o : std_logic_vector(3 downto 0);

    signal top_pc : std_logic_vector(7 downto 0);

    signal cpu_clk : std_logic;
    signal pc_reg, out_reg : std_logic_vector(7 downto 0);
    signal a_reg, b_reg, sum_reg : std_logic_vector(7 downto 0);
    signal mem_addr : std_logic_vector(3 downto 0);
    signal mem_content : std_logic_vector(7 downto 0);
    signal instruction_reg : std_logic_vector(7 downto 0);
    signal mstep_reg : std_logic_vector(7 downto 0);
    signal flags_reg : std_logic_vector(1 downto 0);
    signal cw_reg : std_logic_vector(15 downto 0);

    signal led_strip : std_logic_vector(255 downto 0) := (others => '0');
    signal led_out : std_logic_vector(255 downto 0) := (others => '0');
begin
    -- led_out(15 downto 0) <= led_strip(15 downto 0);
    -- led_out(32 downto 16) <= led_strip(16 to 31);
    -- led_out(47 downto 32) <= led_strip(47 downto 32);
    -- led_out(63 downto 48) <= led_strip(48 to 63);
    -- led_out(79 downto 64) <= led_strip(79 downto 64);
    -- led_out(95 downto 80) <= led_strip(80 to 95);
    -- led_out(111 downto 96) <= led_strip(15 downto 0);
    -- led_out(127 downto 112) <= led_strip(15 downto 0);
    -- led_out(143 downto 128) <= led_strip(15 downto 0);
    -- led_out(159 downto 144) <= led_strip(15 downto 0);
    -- led_out(175 downto 160) <= led_strip(15 downto 0);
    -- led_out(191 downto 176) <= led_strip(15 downto 0);
    -- led_out(207 downto 192) <= led_strip(15 downto 0);
    -- led_out(223 downto 208) <= led_strip(15 downto 0);
    -- led_out(255 downto 240) <= led_strip(15 downto 0);

    wifi_gpio0 <= '1'; -- have to hold high to prevent board from reboot

    map_leds: process(led_strip)
        variable s : integer;
        variable e : integer;
    begin
        for I in 0 to 15 loop
            s := I*16;
            e := I*16 + 15;

            for J in 0 to 15 loop
                if (I mod 2) = 0 then
                    led_out(s+J) <= led_strip(s+J);
                else
                    led_out(s+J) <= led_strip(s+(15-J));
                end if;
            end loop;
        end loop;
    end process;

    addr_int <= to_integer(unsigned(addr) + 1);

    -- led_strip(255) <= '0';
    -- led_strip(254) <= '0';
    -- led_strip(253 downto 247) <= (others => '0');
    -- led_strip(246 downto 239) <= pc_reg;
    -- led_strip(238 downto 223) <= (others => '0');
    -- led_strip(222 downto 215) <= a_reg;
    -- led_strip(214 downto 4) <= (others => '0');
    led_strip(0) <= cpu_clk;
    led_strip(1) <= not cpu_clk;
    led_strip(2) <= cpu_clk;
    led_strip(3) <= not cpu_clk;
    -- led_strip(7 downto 4) <= (others => not cpu_clk);
    led_strip(14 downto 4) <= (others => '0');
    led_strip(15) <= cpu_clk;
    led_strip(16) <= not cpu_clk;
    led_strip(30 downto 17) <= (others => '0');
    led_strip(31) <= cpu_clk;
    led_strip(32) <= not cpu_clk;
    led_strip(255 downto 33) <= (others => '0');

    colour <= x"000055" when led_strip(addr_int) = '1' else x"001000";

    -- state_data <= (
                  -- (state => '1', colour => x"100000"),
                  -- (state => '1', colour => x"001000"),
                  -- (state => '1', colour => x"000010"),
                  -- (state => '1', colour => x"100010")
    -- );

    inst_cpu : entity work.cpu port map
    (
        clk => clk_25mhz,
        rst => btn(1),
        led => led,
        clk_reg => cpu_clk,
        pc_reg => pc_reg,
        a_reg => a_reg
    );

    gp(13 downto 1) <= (others => '0');
    inst_ws : entity work.ws2812b 
    generic map
    (
        NUM_LEDS => 8
    )
    port map
    (
        clk25 => clk_25mhz,
        addr => addr,
        colour => colour,
        dout => gp(0)
    );

end Behave;
