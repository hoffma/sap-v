library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bin2bcd is
    Port (
        bin : in std_logic_vector(7 downto 0);
        hundreds : out std_logic_vector(3 downto 0);
        tens : out std_logic_vector(3 downto 0);
        ones : out std_logic_vector(3 downto 0)
    );
end entity;

architecture Behave of bin2bcd is
begin

    process(bin)
        variable hun : unsigned(3 downto 0) := (others => '0');
        variable ten : unsigned(3 downto 0) := (others => '0');
        variable one : unsigned(3 downto 0) := (others => '0');
    begin
        hun := (others => '0');
        ten := (others => '0');
        one := (others => '0');

        for I in 7 downto 0 loop
            if hun >= to_unsigned(5, hun'length) then
                hun := hun + 3;
            end if;
            if ten >= to_unsigned(5, ten'length) then
                ten := ten + 3;
            end if;
            if one >= to_unsigned(5, one'length) then
                one := one + 3;
            end if;

            hun := hun(2 downto 0) & '0';
            hun(0) := ten(3);
            ten := ten(2 downto 0) & '0';
            ten(0) := one(3);
            one := one(2 downto 0) & '0';
            one(0) := bin(I);
        end loop;

        hundreds <= std_logic_vector(hun);
        tens <= std_logic_vector(ten);
        ones <= std_logic_vector(one);
        
    end process;

end Behave;
