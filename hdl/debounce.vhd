library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debounce is
    Generic (
        DEBOUNCE_SIZE : integer := 32
    );
    Port (
        clk : in std_logic;
        btn_in : in std_logic;
        btn_out : out std_logic
    );
end entity;

architecture Behave of debounce is
    signal debounce_array : std_logic_vector(DEBOUNCE_SIZE-1 downto 0) := (others => '0');
    signal btn_o : std_logic;
begin

    process(clk)
    begin
        if rising_edge(clk) then
            debounce_array <= debounce_array(DEBOUNCE_SIZE-2 downto 0) & btn_in;
        end if;
    end process;

    process(debounce_array)
        variable tmp : std_logic := '1';
    begin
        tmp := debounce_array(DEBOUNCE_SIZE-1);
        for I in DEBOUNCE_SIZE-2 downto 0 loop
            tmp := tmp and debounce_array(I);
        end loop;

        btn_o <= tmp;
    end process;

    btn_out <= btn_o;


end Behave;
