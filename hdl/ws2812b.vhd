library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ws2812b is
    Generic (
        NUM_LEDS : integer := 255
    );
    Port (
        clk25 : in std_logic; -- 25MHz input clock
        -- current address and colour
        addr : out std_logic_vector(7 downto 0);
        colour : in std_logic_vector(23 downto 0);
        -- hardware connection
        dout : out std_logic
    );
end entity;

architecture Behave of ws2812b is
    constant clk_Hz : integer := 25000000;
    constant bpp : integer := 24; -- bits per pixel

    constant BIT_CYCLES : integer := 31;
    constant H0_CYCLES : integer := 10;
    constant H1_CYCLES : integer := 20;
    -- some newer ws2812b require >= 300us reset time
    -- https://www.arrow.com/en/research-and-events/articles/protocol-for-the-ws2812b-programmable-led
    -- 1250 cycles ~ 50us; 7500 cycles ~ 300us @25MHz
    constant RESET_CYCLES : integer := 7600; 
    -- constant RESET_CYCLES : integer := 1250; 

    signal cycle_count : std_logic_vector(23 downto 0) := (others => '0');
    signal bit_count : std_logic_vector(6 downto 0) := (others => '0');
    signal led_count : std_logic_vector(7 downto 0);
    signal curr_bit : std_logic;
    signal tmp_data : std_logic_vector(23 downto 0);

    signal state : std_logic_vector(1 downto 0);
    constant S_RST : std_logic_vector(1 downto 0) := "00";
    constant S_TX  : std_logic_vector(1 downto 0) := "01";
    constant S_PST : std_logic_vector(1 downto 0) := "10";
begin
    addr <= led_count;

    process(clk25)
    begin
        if rising_edge(clk25) then
            cycle_count <= std_logic_vector(unsigned(cycle_count) + 1);

            case state is
                when S_RST =>
                    dout <= '0';
                    if unsigned(cycle_count) >= RESET_CYCLES then
                        state <= S_PST;
                        tmp_data <= colour;
                        cycle_count <= (others => '0');
                        bit_count <= (others => '0');
                        led_count <= (others => '0');
                    end if;

                when S_TX =>
                    dout <= '0';
                    if (curr_bit = '1' and unsigned(cycle_count) <= H1_CYCLES) then
                        dout <= '1';
                    elsif (curr_bit = '0' and unsigned(cycle_count) <= H0_CYCLES) then
                        dout <= '1';
                    elsif (unsigned(cycle_count) >= BIT_CYCLES) then
                        state <= S_PST;
                        cycle_count <= (others => '0');
                    end if;

                when S_PST =>
                    if (unsigned(bit_count) >= 23 and unsigned(led_count) < (NUM_LEDS-1)) then
                        bit_count <= (others => '0');
                        state <= S_TX;
                        led_count <= std_logic_vector(unsigned(led_count) + 1);
                        tmp_data <= colour;
                    elsif (unsigned(bit_count) >= 23 and unsigned(led_count) >= (NUM_LEDS-1)) then
                        state <= S_RST;
                        cycle_count <= (others => '0');
                    else
                        state <= S_TX;
                        curr_bit <= tmp_data(0);
                        tmp_data <= '0' & tmp_data(23 downto 1);
                        -- curr_bit <= colour(to_integer(23 - unsigned(bit_count)));
                        bit_count <= std_logic_vector(unsigned(bit_count) + 1);
                    end if;

                when others =>
                    state <= S_RST;
                    cycle_count <= (others => '0');
                    bit_count <= (others => '0');
                    led_count <= (others => '0');
                end case;
        end if;
    end process;

end Behave;
