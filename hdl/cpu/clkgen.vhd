-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clkgen is
    Port (
        clk_in : in std_logic;
        nEn : in std_logic; -- not enable
        mode : in std_logic; -- mode 0: clk_out = clk_in/clkdv; mode 1: manual clock
        step : in std_logic; -- manual clock step control
        clkdv : in std_logic_vector(31 downto 0);
        clk_out : out std_logic
    );
end entity;

architecture Behave of clkgen is
    signal div_count : std_logic_vector(31 downto 0) := (others => '0');
    signal clk_o : std_logic := '0';
    signal debounced_step, prev_step : std_logic;
begin

    inst_debounce : entity work.debounce 
    generic map (
        DEBOUNCE_SIZE => 32
    )
    port map (
        clk => clk_in,
        btn_in => step,
        btn_out => debounced_step
    );

    clk_out <= clk_o;

    process(clk_in)
    begin
        if rising_edge(clk_in) then
            prev_step <= debounced_step;

            if nEn = '0' then
                div_count <= (others => '0');
                if mode = '1' then
                    if prev_step = '0' and debounced_step = '1' then
                        clk_o <= '1';
                    else
                        clk_o <= '0';
                    end if;

                else
                    div_count <= std_logic_vector(unsigned(div_count) + 1);

                    if unsigned(div_count) >= unsigned(clkdv)/2 then
                        clk_o <= not clk_o;
                        div_count <= (others => '0');
                    end if;
                end if;
            end if;
        end if;
    end process;

end Behave;
