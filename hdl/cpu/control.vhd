-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- HLT, halt the processor
-- MI, memory in

-- RI, 
-- RO,

-- IO, instruction register out
-- II, instruction register in

-- AI, register A in
-- AO, register A out

-- EO, Sum register of ALU out
-- SU, Addition or substraction signal for ALU

-- BI, register B input
-- BO, reigster B output

-- CE
-- CO

-- J, jump signal
entity control is
    Generic (
        DATA_WIDTH : integer := 8;
        ADDR_WIDTH : integer := 4
    );
    Port (
        clk : in std_logic;
        rst : in std_logic;
        instr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
        cf : in std_logic;
        zf : in std_logic;

        -- output control signals
        HLT : out std_logic;
        MI : out std_logic;
        RO : out std_logic;
        RI : out std_logic;
        IO : out std_logic;
        II : out std_logic;
        AO : out std_logic;
        AI : out std_logic;
        EO : out std_logic;
        SU : out std_logic;
        BI : out std_logic;
        OI : out std_logic;
        CE : out std_logic;
        CO : out std_logic;
        JP : out std_logic;
        FI : out std_logic
    );
end entity;


architecture Behave of control is
    constant NOP : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"0";
    constant LDA : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"1";
    constant ADD : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"2";
    constant SUB : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"3";
    constant STA : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"4";
    constant LDI : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"5";
    constant JMP : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"6";
    constant JC  : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"7";
    constant JZ  : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"8";
    constant DOUT : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"e";
    constant HALT : std_logic_vector(ADDR_WIDTH-1 downto 0) := x"f";

    -- each instruction has 5 micro instructions
    signal uinstr_count : std_logic_vector(2 downto 0) := (others => '0'); 

    signal current_instr : std_logic_vector(ADDR_WIDTH-1 downto 0);
begin


    process(clk)
    begin
        if rising_edge(clk) then
            uinstr_count <= std_logic_vector(unsigned(uinstr_count) + 1);
            HLT <= '0';
            MI <= '0';
            RO <= '0';
            RI <= '0';
            IO <= '0';
            II <= '0';
            AO <= '0';
            AI <= '0';
            EO <= '0';
            SU <= '0';
            BI <= '0';
            OI <= '0';
            CE <= '0';
            CO <= '0';
            JP <= '0';
            FI <= '0';

            case uinstr_count is
                when "000" => -- fetch cycle
                    -- this loads the current program counter into the memory 
                    -- address register
                    CO <= '1'; -- instruction counter out
                    MI <= '1'; -- memory address register in
                    current_instr <= instr;
                when "001" =>
                    RO <= '1'; -- RAM outputs instruction onto bus
                    II <= '1'; -- instruction register reads instruction
                    -- increment the program counter
                    CE <= '1';
                -- these get handlded below
                when "010" =>
                when "011" =>
                when "100" =>
                    -- start with next instruction, on next cycle
                    uinstr_count <= (others => '0');
                when others =>
                    uinstr_count <= (others => '0');
            end case;

            if current_instr = NOP then
            elsif current_instr = LDA then
                if uinstr_count = "010" then
                    IO <= '1';
                    MI <= '1';
                elsif uinstr_count = "011" then
                    RO <= '1';
                    AI <= '1';
                end if;
            elsif current_instr = ADD then
                if uinstr_count = "010" then
                    IO <= '1';
                    MI <= '1';
                elsif uinstr_count = "011" then
                    RO <= '1';
                    BI <= '1';
                elsif uinstr_count = "100" then
                    EO <= '1';
                    AI <= '1';
                    FI <= '1';
                end if;
            elsif current_instr = SUB then
                if uinstr_count = "010" then
                    IO <= '1';
                    MI <= '1';
                elsif uinstr_count = "011" then
                    RO <= '1';
                    BI <= '1';
                elsif uinstr_count = "100" then
                    EO <= '1';
                    AI <= '1';
                    FI <= '1';
                end if;
            elsif current_instr = STA then
                if uinstr_count = "010" then
                    IO <= '1';
                    MI <= '1';
                elsif uinstr_count = "011" then
                    AO <= '1';
                    RI <= '1';
                end if;
            elsif current_instr = LDI then
                if uinstr_count = "010" then
                    IO <= '1';
                    AI <= '1';
                end if;
            elsif current_instr = JMP then
                if uinstr_count = "010" then
                    IO <= '1';
                    JP <= '1';
                end if;
            elsif current_instr = JC  then
                -- only do something if the carry flag is set
                if (uinstr_count = "010") and (CF = '1') then
                    IO <= '1';
                    JP <= '1';
                end if;
            elsif current_instr = JZ  then
                -- only do something if the zero flag is set
                if (uinstr_count = "010") and (ZF = '1') then
                    IO <= '1';
                    JP <= '1';
                end if;
            elsif current_instr = DOUT then
                if uinstr_count = "010" then
                    AO <= '1';
                    OI <= '1';
                end if;
            elsif current_instr = HALT then
                if uinstr_count = "010" then
                    HLT <= '1';
                end if;
            end if;

            if rst = '1' then
                uinstr_count <= (others => '0');
                HLT <= '0';
                MI <= '0';
                RO <= '0';
                RI <= '0';
                IO <= '0';
                II <= '0';
                AO <= '0';
                AI <= '0';
                EO <= '0';
                SU <= '0';
                BI <= '0';
                OI <= '0';
                CE <= '0';
                CO <= '0';
                JP <= '0';
            end if;
        end if;
    end process;

end Behave;

