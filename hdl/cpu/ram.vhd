-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;


-- TODO: FIX READ AND WRITE FROM AND TO RAM
entity RAM is
    Generic (
        DATA_WIDTH : integer := 8;
        ADDR_WIDTH : integer := 4
    );
    Port (
        clk : in std_logic;
        rst : in std_logic;
        oe : in std_logic; -- output enable
        we : in std_logic; -- write enable
        addr : in std_logic_vector(ADDR_WIDTH-1 downto 0);
        -- data : inout std_logic_vector(DATA_WIDTH-1 downto 0)
        din : in std_logic_vector(DATA_WIDTH-1 downto 0);
        dout : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end entity;

architecture Behave of ram is
    signal dout_internal : std_logic_vector(DATA_WIDTH-1 downto 0);

    type ram_t is array (0 to 15) of bit_vector(DATA_WIDTH-1 downto 0);
    impure function InitRamFromFile(RamFileName : in string) return ram_t is
        FILE RamFile : text open read_mode is RamFileName;
        variable RamFileLine : line;
        variable RAM : ram_t;
    begin
        for I in ram_t'range loop
            readline(RamFile, RamFileLine);
            read(RamFileLine, RAM(I));
        end loop;
        return RAM;
    end function;

    signal ram_data : ram_t := InitRamFromFile("firmware.dat"); 
begin

    dout <= dout_internal when (oe = '1' and we = '0') else (others => 'Z');

    dout_internal <= to_stdlogicvector(ram_data(to_integer(unsigned(addr))));

    process(clk, addr, oe, we)
    begin
        if rising_edge(clk) then

            if (oe = '0' and we = '1') then
                -- din_internal <= data;
                ram_data(to_integer(unsigned(addr))) <= to_bitvector(din);
            end if;
        end if;
    end process;

end Behave;
