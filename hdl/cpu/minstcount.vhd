-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity minstcount is
    Port (
        nclk : in std_logic;
        rst : in std_logic;
        en : in std_logic;
        mcnt : out std_logic_vector(2 downto 0);
        step : out std_logic_vector(4 downto 0)
    );
end entity;

architecture Behav of minstcount is
    signal count : std_logic_vector(2 downto 0) := (others => '0');
begin

    step(0) <= not (count(0) or count(1) or count(2));
    step(1) <= count(0) and not count(1) and not count(2);
    step(2) <= count(1) and not count(0) and not count(2);
    step(3) <= count(0) and count(1) and not count(2);
    step(4) <= count(2) and not count(0) and not count(1);
    mcnt <= count;

    process(nclk)
    begin
        if rising_edge(nclk) then
            if en = '1' then
                count <= std_logic_vector(unsigned(count) + 1);

                if count = "100" then
                    count <= (others => '0');
                end if;
            end if;

            if rst = '1' then
                count <= (others => '0');
            end if;
        end if;
    end process;
end Behav;
