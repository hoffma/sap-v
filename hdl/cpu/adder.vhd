-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;

-- half adder
entity halfadd is
    port (
        a, b : in std_logic;
        y : out std_logic;
        c : out std_logic
    );
end entity;

architecture Behav of halfadd is

begin
    y <= a xor b;
    c <= a and b;
end Behav;


library ieee;
use ieee.std_logic_1164.all;

-- full adder
entity fulladd is
    port (
        a, b, ci : in std_logic;
        y : out std_logic;
        c : out std_logic
    );
end entity;

architecture Behav of fulladd is
    signal y1, c1, c2 : std_logic;
begin
    inst_ha1 : entity work.halfadd port map
    (
        a => a,
        b => b,
        y => y1,
        c => c1
    );

    inst_ha2 : entity work.halfadd port map
    (
        a => ci,
        b => y1,
        y => y,
        c => c2
    );

    c <= c1 or c2;

end Behav;
