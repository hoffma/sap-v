-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;

entity reg is
    generic (
        DATA_WIDTH : integer := 8
    );
    port (
        clk : in std_logic;
        rst : in std_logic;
        wr  : in std_logic;
        din     : in std_logic_vector(DATA_WIDTH-1 downto 0);
        dout    : out std_logic_vector(DATA_WIDTH-1 downto 0)
     );
end entity;


architecture Behav of reg is
    -- internal data
    signal data_int : std_logic_vector(DATA_WIDTH-1 downto 0);
begin

    -- read data into the register if wr signal is set
    -- clocked reset resets data to zero
    process(clk)
    begin
        if rising_edge(clk) then
            if wr = '1' then
                dout <= din;
            end if;

            if rst = '1' then
                dout <= (others => '0');
            end if;
        end if;
    end process;

end Behav;
