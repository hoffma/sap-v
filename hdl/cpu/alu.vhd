-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
    generic (
        DATA_WIDTH : integer := 8
    );
    port (
        clk : in std_logic;
        cs : in std_logic;
        ans : in std_logic; -- add, not subtract
        a, b : in std_logic_vector(DATA_WIDTH-1 downto 0);
        zf, cf : out std_logic; -- zero flag, carry flag
        y : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end entity;

architecture Behav of alu is
    signal s_b, neg_b : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal data_out : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal s_y : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal s_c : std_logic_vector(DATA_WIDTH downto 0);
    signal s_zf, s_cf : std_logic;
begin

    y <= s_y; -- when cs = '1' else (others => 'Z');

    neg_b <= std_logic_vector(unsigned(not b) + 1);
    s_b <= b when ans = '0' else neg_b;

    process(clk)
    begin
        if rising_edge(clk) then
            -- data_out <= s_y;
        end if;
    end process;

    s_c(0) <= '0'; -- no carry in on first stage

    add_gen : for I in 0 to DATA_WIDTH-1 generate
        fa: entity work.fulladd port map
        (
            a => a(i),
            b => s_b(i),
            ci => s_c(i),
            y => s_y(i),
            c => s_c(i+1)
        );
    end generate;

    s_cf <= '0' when s_zf = '1' else s_c(DATA_WIDTH);
    s_zf <= '1' when (s_y = x"00" )else '0';

    cf <= s_cf;
    zf <= s_zf;


end Behav;
