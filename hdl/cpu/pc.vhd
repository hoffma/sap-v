-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc is
    generic (
        ADDR_WIDTH : integer := 4; 
        DATA_WIDTH : integer := 8
    );
    port (
        clk : in std_logic;
        rst : in std_logic;
        en : in std_logic;
        cs : in std_logic;
        jmp : in std_logic;
        jaddr : in std_logic_vector(DATA_WIDTH-1 downto 0);
        pc_out : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end entity;

architecture Behav of pc is
    signal pc_pc : std_logic_vector(ADDR_WIDTH-1 downto 0);
begin

    pc_out <= ("0000" & pc_pc) when cs = '1' else (others => 'Z');

    process(clk)
    begin
        if rising_edge(clk) then
            if en = '1' then
                pc_pc <= std_logic_vector(unsigned(pc_pc) + 1);
            end if;

            if jmp = '1' then
                pc_pc <= jaddr(3 downto 0);
            end if;

            if rst = '1' then
                pc_pc <= (others => '0');
            end if;
        end if;
    end process;

end Behav;
