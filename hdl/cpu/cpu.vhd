-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cpu is
    Generic (
        ADDR_WIDTH : integer := 4;
        DATA_WIDTH : integer := 8
    );
    Port (
        clk : in std_logic;
        rst : in std_logic;

        led : out std_logic_vector(7 downto 0); -- 7 segment output
        -- for register visualizations
        clk_reg : out std_logic;
        mem_addr : out std_logic_vector(3 downto 0);
        mem_content : out std_logic_vector(7 downto 0);
        instruction_reg : out std_logic_vector(7 downto 0);
        mstep_reg : out std_logic_vector(7 downto 0);
        flags_reg : out std_logic_vector(1 downto 0);
        pc_reg : out std_logic_vector(7 downto 0);
        a_reg : out std_logic_vector(7 downto 0);
        b_reg : out std_logic_vector(7 downto 0);
        sum_reg : out std_logic_vector(7 downto 0);
        cw_reg : out std_logic_vector(15 downto 0);
        out_reg : out std_logic_vector(7 downto 0)
    );
end entity;


architecture Behave of cpu is
    signal clkdiv : std_logic_vector(31 downto 0);
    signal cpu_clk, cpu_nclk, clk_step, clk_mode : std_logic;

    signal instruction, data_bus : std_logic_vector(7 downto 0);

    signal micro_cnt : std_logic_vector(2 downto 0);
    signal micro_step : std_logic_vector(4 downto 0);

    signal ram_addr : std_logic_vector(3 downto 0);
    signal opc : std_logic_vector(3 downto 0); -- current instruction
    signal data_nibble : std_logic_vector(7 downto 0);
    signal control_word : std_logic_vector(15 downto 0);
    signal carry, zero : std_logic;
    signal flags_out, flag_reg : std_logic_vector(1 downto 0);

    signal alu_a, alu_b, alu_y, ra_out : std_logic_vector(7 downto 0);
    signal pc_pc : std_logic_vector(7 downto 0);
    signal ram_out : std_logic_vector(7 downto 0);

    -- Control word signals
    signal HLT : std_logic;
    signal MI : std_logic;
    signal RO : std_logic;
    signal RI : std_logic;
    signal IO : std_logic;
    signal II : std_logic;
    signal AO : std_logic;
    signal AI : std_logic;
    signal EO : std_logic;
    signal SU : std_logic;
    signal BI : std_logic;
    signal OI : std_logic;
    signal CE : std_logic;
    signal CO : std_logic;
    signal JP : std_logic;
    signal FI : std_logic;
begin
    HLT <= control_word(0);
    MI <= control_word(1);
    RO <= control_word(2);
    RI <= control_word(3);
    IO <= control_word(4);
    II <= control_word(5);
    AO <= control_word(6);
    AI <= control_word(7);
    EO <= control_word(8);
    SU <= control_word(9);
    BI <= control_word(10);
    OI <= control_word(11);
    CE <= control_word(12);
    CO <= control_word(13);
    JP <= control_word(14);
    FI <= control_word(15);

    -- clkdiv <= x"00000002"; -- simulation test value
    clkdiv <= x"00f00000";
    cpu_nclk <= not cpu_clk;
    clk_mode <= '0';
    clk_step <= '0';

    process(clk)
    begin
        if rising_edge(clk) then
            clk_reg <= cpu_clk;
            pc_reg <= pc_pc;
            a_reg <= ra_out;
        end if;
    end process;


    inst_clk : entity work.clkgen 
    Port map (
        clk_in => clk,
        nEn => HLT,
        mode => clk_mode,
        step => clk_step,
        clkdv => clkdiv,
        clk_out => cpu_clk
    );

    inst_pc : entity work.pc
    Port map(
        clk => cpu_clk,
        rst => rst,
        en => CE,
        cs => CO,
        jmp => JP,
        jaddr => data_bus,
        pc_out => pc_pc 
    );

    isnt_mcount : entity work.minstcount
    Port map (
        nclk => cpu_nclk,
        rst => rst,
        en => '1',
        mcnt => micro_cnt,
        step => micro_step -- step output ignored for now. Just for display purpose
    );

    inst_MAR : entity work.reg 
    generic map (
        DATA_WIDTH => 4
    )
    port map (
        clk => cpu_clk,
        rst => rst,
        wr => MI,
        din =>  data_bus(3 downto 0),
        dout => ram_addr
    );

    inst_RAM : entity work.ram
    port map (
        clk => cpu_clk,
        rst => rst,
        oe => RO,
        we => RI,
        addr => ram_addr,
        din => data_bus,
        dout => ram_out
    );

    inst_IR : entity work.reg
    port map (
        clk => cpu_clk,
        rst => rst,
        wr => II,
        din => data_bus,
        dout => instruction
    );

    inst_idecode : entity work.idecode
    port map (
        instr => opc,
        mcnt => micro_cnt,
        zf => flag_reg(0),
        cf => flag_reg(1),
        cw => control_word
    );

    inst_RA : entity work.reg
    port map (
        clk => cpu_clk,
        rst => rst,
        wr => AI,
        din => data_bus,
        dout => ra_out
    );

    inst_RB : entity work.reg
    port map (
        clk => cpu_clk,
        rst => rst,
        wr => BI,
        din => data_bus,
        dout => alu_b
    );

    inst_ALU : entity work.alu
    port map (
        clk => cpu_clk,
        cs => '1',
        ans => SU,
        a => alu_a,
        b => alu_b,
        zf => zero,
        cf => carry,
        y => alu_y
    );

    inst_OUT : entity work.reg
    port map (
        clk => cpu_clk,
        rst => rst,
        wr => OI,
        din => data_bus,
        dout => led
    );

    flags_out <= carry & zero; 
    inst_FLAG : entity work.reg
    generic map (
        DATA_WIDTH => 2
    )
    port map (
        clk => cpu_clk,
        rst => rst,
        wr => FI,
        din => flags_out,
        dout => flag_reg
    );

    -- misc
    opc <= instruction(7 downto 4);
    data_nibble <= "0000" & instruction(3 downto 0);
    alu_a <= ra_out;
    data_bus <= data_nibble when (IO = '1') else 
                ra_out when (AO = '1') else 
                alu_y when (EO = '1') else
                pc_pc when (CO = '1') else
                ram_out when (RO = '1') else
                (others => 'Z');

end Behave;
