-- Author: Mario Hoffmann, 2021

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity idecode is
    Port (
        instr : in std_logic_vector(3 downto 0);
        mcnt : in std_logic_vector(2 downto 0);
        zf : in std_logic;
        cf : in std_logic;
        cw : out std_logic_vector(15 downto 0)
    );
end entity;


architecture Behav of idecode is
    -- microcode mapping https://dangrie158.github.io/SAP-1/isa.html#control-word
    constant HLT : std_logic_vector(15 downto 0) := x"0001";
    constant MI  : std_logic_vector(15 downto 0) := x"0002";
    constant RO  : std_logic_vector(15 downto 0) := x"0004";
    constant RI  : std_logic_vector(15 downto 0) := x"0008";
    constant IO  : std_logic_vector(15 downto 0) := x"0010";
    constant II  : std_logic_vector(15 downto 0) := x"0020";
    constant AO  : std_logic_vector(15 downto 0) := x"0040";
    constant AI  : std_logic_vector(15 downto 0) := x"0080";
    constant EO  : std_logic_vector(15 downto 0) := x"0100";
    constant SU  : std_logic_vector(15 downto 0) := x"0200";
    constant BI  : std_logic_vector(15 downto 0) := x"0400";
    constant OI  : std_logic_vector(15 downto 0) := x"0800";
    constant CE  : std_logic_vector(15 downto 0) := x"1000";
    constant CO  : std_logic_vector(15 downto 0) := x"2000";
    constant JP  : std_logic_vector(15 downto 0) := x"4000";
    constant FI  : std_logic_vector(15 downto 0) := x"8000";

    constant JC_INSTR : std_logic_vector(3 downto 0) := "0111";
    constant JZ_INSTR : std_logic_vector(3 downto 0) := "1000";

    signal JC, JZ : std_logic_vector(15 downto 0);
    signal rom_out : std_logic_vector(15 downto 0);
    signal addr : std_logic_vector(6 downto 0);

    type rom_t is array(0 to 127) of std_logic_vector(15 downto 0);
    constant instr_rom : rom_t := (
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000", -- NOP
        CO or MI, RO or II or CE, IO or MI, RO or AI, x"0000", x"0000", x"0000", x"0000", -- LDA
        CO or MI, RO or II or CE, IO or MI, RO or BI, EO or AI or FI, x"0000", x"0000", x"0000", -- ADD
        CO or MI, RO or II or CE, IO or MI, RO or BI, EO or SU or AI or FI, x"0000", x"0000", x"0000", -- SUB
        CO or MI, RO or II or CE, IO or MI, AO or RI, x"0000", x"0000", x"0000", x"0000", -- STA
        CO or MI, RO or II or CE, IO or AI, x"0000", x"0000", x"0000", x"0000", x"0000", -- LDI
        CO or MI, RO or II or CE, IO or JP, x"0000", x"0000", x"0000", x"0000", x"0000", -- JMP
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000", -- JC
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000", -- JZ
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000",
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000",
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000",
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000",
        CO or MI, RO or II or CE, x"0000", x"0000", x"0000", x"0000", x"0000", x"0000",
        CO or MI, RO or II or CE, AO or OI, x"0000", x"0000", x"0000", x"0000", x"0000", -- OUT
        CO or MI, RO or II or CE, HLT, x"0000", x"0000", x"0000", x"0000", x"0000" -- HLT
    );
begin

    JZ <= (IO or JP) when zf = '1' else (others => '0');
    JC <= (IO or JP) when cf = '1' else (others => '0');
    
    

    addr <= instr & mcnt;
    rom_out <= instr_rom(to_integer(unsigned(addr)));
    cw <= (rom_out or JC) when (instr = JC_INSTR and mcnt = "010") else
          (rom_out or JZ) when (instr = JZ_INSTR and mcnt = "010") else
          rom_out;
    

end Behav;
