library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity seven_seg is
    Generic (
        DIGIT_DIV : integer := 400
    );
    Port (
        clk25 : in std_logic;
        bin : in std_logic_vector(7 downto 0);
        digit : out std_logic_vector(3 downto 0);
        bcd : out std_logic_vector(3 downto 0)
    );
end entity;

architecture Behave of seven_seg is
    signal counter : unsigned(31 downto 0) := (others => '0');
    signal s_state : std_logic_vector(1 downto 0) := "00";
    signal hundred, ten, one : std_logic_vector(3 downto 0);
begin

    inst_bcd : entity work.bin2bcd
    port map (
        bin => bin,
        hundreds => hundred,
        tens => ten,
        ones => one
    );

    process(clk25)
    begin
        if rising_edge(clk25) then
            counter <= counter + 1;

            case s_state is
                when "00" =>
                    digit <= "1110";
                    bcd <= one;
                    if counter >= DIGIT_DIV then
                        s_state <= "01";
                        counter <= (others => '0');
                    end if;

                when "01" =>
                    digit <= "1101";
                    bcd <= ten;
                    if counter >= DIGIT_DIV then
                        s_state <= "10";
                        counter <= (others => '0');
                    end if;

                when "10" =>
                    digit <= "1011";
                    bcd <= hundred;
                    if counter >= DIGIT_DIV then
                        s_state <= "00";
                        counter <= (others => '0');
                    end if;

                when others =>
                    s_state <= "00";
                    counter <= (others => '0');
                end case;
        end if;
    end process;

end Behave;
