# VHDL top with instantiated Verilog
PROJECT := SAP
TOPMOD := top
# other files
VHDL_SYN_FILES += debounce.vhd bin2bcd.vhd seven_seg.vhd ws2812b.vhd
VHDL_SYN_FILES += cpu/ram.vhd cpu/clkgen.vhd cpu/idecode.vhd cpu/minstcount.vhd cpu/pc.vhd cpu/adder.vhd cpu/alu.vhd cpu/reg.vhd cpu/cpu.vhd
VHDL_SYN_FILES += top.vhd
# CPU files
VERILOG_SYN_FILES = 
VHDL_FILES=$(patsubst %, hdl/%, $(VHDL_SYN_FILES))
VERILOG_FILES=$(patsubst %, hdl/%, $(VERILOG_SYN_FILES))
SIMMOD := $(TOPMOD)_tb
TBFILE := tb/$(SIMMOD).vhd
SIMFILE := $(TOPMOD)_sim.ghk

# Verilog top with instantiated VHDL
#VHDL_SYN_FILES = ../../vhdl/sb_ice40_components.vhd clkgen.vhd
#VERILOG_SYN_FILES = blink.v

GHDL_FLAGS += --std=08
GHDL      ?= ghdl
GHDLSYNTH ?= ghdl
YOSYS     ?= yosys

# Default target: run all required targets to build the DFU image.
all: blink.bit
	@true

.DEFAULT: all

# Use *Yosys* to generate the synthesized netlist.
# This is called the **synthesis** and **tech mapping** step.
blink.json: $(VHDL_FILES) $(VERILOG_FILES)
	$(YOSYS) \
		-p \
		"$(GHDLSYNTH) $(GHDL_FLAGS) $(VHDL_FILES) -e top; \
		synth_ecp5 \
		-top top \
		-json $@" $(VERILOG_FILES) 2>&1 | tee yosys-report.txt

ulx3s_out.config: blink.json
	nextpnr-ecp5 -l "pnr.log" --45k --json blink.json --package CABGA381 --lpf ulx3s_v20.lpf --textcfg ulx3s_out.config

$(PROJECT).bit: ulx3s_out.config
	ecppack --compress --freq 62.0 ulx3s_out.config $(PROJECT).bit

# Use df-util to load the DFU image onto the Fomu.
prog: $(PROJECT).bit
	fujprog $<

analyze:
	ghdl -a $(VHDL_FILES) $(TBFILE)

sim: analyze
	# ghdl -a $(VHDL_FILES) $(TBFILE)
	ghdl -r $(SIMMOD) --stop-time=500us --vcd=$(SIMFILE)
	# ghdl -r $(SIMMOD) --vcd=$(SIMFILE)

.PHONY: prog

# Cleanup the generated files.
clean:
	rm -fr *.cf *.json *-report.txt *.asc *.bit *.dfu
	rm -rf *.ghk
	rm -rf *.log *.config

.PHONY: clean
